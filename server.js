const express = require('express')
const app = express()
const port = 8080

app.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*');
    next();
});

app.get('/', (req, res) => res.send('Hello World!'))
app.get('/error500', function(req, res) {
    throw {status: 500, message: 'Error 500'};
});
app.get('/error300', function(req, res) {
    res.sendStatus(300)
});
app.get('/error400', function(req, res) {
    res.sendStatus(400)
});

app.get('/timeout', function(req, res) {
    test(res)
});
app.listen(port, () => console.log(`Example app listening on port ${port}!`))

async function test(res) {
    setTimeout(function() {
        res.send('5000 milliseconds timeout')
    }, 5000);
}


